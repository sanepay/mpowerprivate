package mpowerprivate

// Response from MPower
type Response struct {
	Status  string `json:"status"`
	Message string `json:"message"`
}

// IsOK verifies Status is "OK"
func (r *Response) IsOK() bool {
	return r.Status == "OK"
}

type UserAccountCheckResponse struct {
	*Response
	Data *UserAccountCheckResponseData
}

type UserAccountCheckResponseData struct {
	AccountAlias     string `json:"account_alias"`
	AccountType      string `json:"account_type"`
	ActualBalance    string `json:"actual_balance"`
	AvailableBalance string `json:"available_balance"`
	Name             string `json:"name"`
	Email            string `json:"email"`
	MobileNumber     string `json:"mobileno"`
	EmailConfirmed   bool   `json:"email_confirmed"`
	PhoneConfirmed   bool   `json:"phone_confirmed"`
}
