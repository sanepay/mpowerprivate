package mpowerprivate

import (
	"errors"
	"fmt"
	"net/http"

	"github.com/jmcvetta/napping"
	"github.com/kr/pretty"
)

const (
	baseURL = "https://app.mpowerpayments.com"
)

// EndPointType to be accessed.
type EndpointType int

// Endpoint types
const (
	EndpointPartner = iota + 1
	EndpointUssd
	EndpointMobile
)

func (e EndpointType) string() string {
	switch e {
	case EndpointPartner:
		return "partner-api"
	case EndpointUssd:
		return "ussd-api"
	case EndpointMobile:
		return "mobile-api"
	default:
		return StrEmpty
	}
}

// MPowerPrivate is the main actor.
type MPowerPrivate struct {
	apiToken     string
	baseAddress  string
	AccountAlias string
}

// New MPowerPrivate
func New(endpoint EndpointType, apiToken, accountAlias string) *MPowerPrivate {
	address := fmt.Sprintf("%v/%v/", baseURL, endpoint.string())
	return &MPowerPrivate{
		baseAddress:  address,
		apiToken:     apiToken,
		AccountAlias: accountAlias,
	}
}

func (mp *MPowerPrivate) post(url string, payload interface{},
	result interface{}) error {

	header := make(http.Header)
	header.Add("Accept", "application/json")
	header.Add("Content-Type", "application/json")
	header.Add("Api-Token", mp.apiToken)
	session := &napping.Session{Header: &header}

	res, err := session.Post(url, payload, result, nil)
	if err != nil {
		return err
	}

	pretty.Printf("status: %v\n", res.Status())
	pretty.Printf("raw text: %v\n", res.RawText())
	return nil
}

// UserAccountCheck checks the user's account details.
func (mp *MPowerPrivate) UserAccountCheck(
	unlockKey string) (*UserAccountCheckResponse, error) {

	payload := &struct {
		AccountAlias string `json:"account_alias"`
		UnlockKey    string `json:"unlock_key"`
	}{
		AccountAlias: mp.AccountAlias,
		UnlockKey:    unlockKey,
	}
	result := new(UserAccountCheckResponse)
	err := mp.post(mp.baseAddress+"useraccount/checkaccount", payload, result)
	if err != nil {
		return nil, err
	}
	if result.IsOK() {
		return result, nil
	}
	return nil, errors.New(result.Message)
}

func (mp *MPowerPrivate) SendMoneyMPower(account string,
	amount float64, pin string) (*Response, error) {

	payload := &struct {
		AccountAlias string  `json:"account_alias"`
		Amount       float64 `json:"amount"`
		PayeeAccount string  `json:"payee_account"`
		Pin          string  `json:"pin"`
	}{
		AccountAlias: mp.AccountAlias,
		Amount:       amount,
		PayeeAccount: account,
		Pin:          pin,
	}
	result := new(Response)
	err := mp.post(mp.baseAddress+"sendmoney/mpower", payload, result)
	if err != nil {
		return nil, err
	}
	if result.IsOK() {
		return result, nil
	}
	return nil, errors.New(result.Message)
}
