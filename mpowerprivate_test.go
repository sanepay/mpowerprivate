package mpowerprivate

import (
	"testing"

	"github.com/stretchr/testify/suite"
)

type MPowerPrivateSuite struct {
	suite.Suite
	*MPowerPrivate
}

func (m *MPowerPrivateSuite) SetupSuite() {
	m.MPowerPrivate = New(EndpointUssd, "05f0af8310355dcb17a7b0ea3bd470f0",
		"0246662003")
}

func (m *MPowerPrivateSuite) TestUserAccountCheck() {
	m.AccountAlias = "0246662003"
	result, err := m.UserAccountCheck("6KZ4HJFEDG6XHKVKPX9Q2ZYTLZ4CV4QG")
	m.Nil(err)
	m.NotNil(result)
	m.Equal("0246662003", result.Data.MobileNumber)
}

// func (m *MPowerPrivateSuite) TestSendMoneyMPower() {
// 	m.AccountAlias = "0208400654"
// 	result, err := m.SendMoneyMPower("0246662003", 1, "9422")
// 	m.Nil(err)
// 	m.NotNil(result)
// 	m.True(result.IsOK())
// }

func TestMPowerPrivateSuite(t *testing.T) {
	suite.Run(t, new(MPowerPrivateSuite))
}
